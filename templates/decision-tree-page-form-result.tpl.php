<?php
print '<h1>' . t('Here are your results:') . '</h1>';
print '<ul>';
foreach (element_children($form['results']) as $nid) {
  print '<li>' . drupal_render($form['results'][$nid]) . '</li>';
}
print '</ul>';
print drupal_render($form);